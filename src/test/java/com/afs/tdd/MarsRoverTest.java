package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {
    
    @Test
    void should_change_location_to_0_1_North_when_executeCommand_given_Location_0_0_North_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(1, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_1_North_when_executeCommand_given_Location_0_0_North_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West, marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_1_North_when_executeCommand_given_Location_0_0_North_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        assertEquals(0, marsRover.getLocation().getCoordinateX());
        assertEquals(0, marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East, marsRover.getLocation().getDirection());
    }
}
