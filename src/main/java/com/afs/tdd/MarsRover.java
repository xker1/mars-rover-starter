package com.afs.tdd;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)){
            move();
        }
        if (command.equals(Command.Left)){
            turnLeft();
        }
        if (command.equals(Command.Right)){
            turnRight();
        }
    }

    private void turnRight() {
        if (location.getDirection().equals(Direction.North))
            location.setDirection(Direction.East);
    }

    private void turnLeft() {
        if (location.getDirection().equals(Direction.North))
            location.setDirection(Direction.West);
    }

    private void move() {
        location.setCoordinateY(location.getCoordinateY() + 1);
    }

    public Location getLocation() {
        return this.location;
    }
}
